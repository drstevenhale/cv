# Dr Steven J. Hale CSci

[![pipeline status](https://gitlab.com/drstevenhale/cv/badges/main/pipeline.svg)](https://gitlab.com/drstevenhale/cv/commits/main)

The latest version can be downloaded [here](https://gitlab.com/drstevenhale/cv/-/jobs/artifacts/main/raw/cv.pdf?job=compile_pdf).

CV design based on [AltaCV](https://github.com/liantze/AltaCV), yet
another LaTeX CV/Résumé class v1.7.1 by LianTze Lim
(liantze@gmail.com), and inspired by
[enhancv.com](https://enhancv.com).
