#
#  MAKEFILE
#
#    Steven Hale
#    2019 August 7
#    Birmingham, UK
#

################################################################

main = cv

svg := $(wildcard *.svg)
pdf := $(filter-out $(main).pdf, $(wildcard *.pdf))
png := $(wildcard *.png)
jpg := $(wildcard *.jpg)
tex := $(wildcard *.tex)

################################################################

.PHONY: all help clean spell check view

all: $(main).pdf

help :
	@echo "Targets:"
	@echo "all        Make the PDF version of the CV."
	@echo "clean      Clean up."
	@echo "spell      Spell-check the source file."
	@echo "check      Search the build log for various warnings."
	@echo "view       View the document."
	@echo "linear     Optimise for web download."
	@echo "meta       View the document metadata."

clean :
	git clean -dxf

spell : $(main).ltx $(tex)
	$(foreach                        \
	   file,                         \
	   $^,                           \
	   aspell                        \
              --lang=en_GB               \
	      --personal=./aspell.en.pws \
	      --repl=./aspell.en.prepl   \
	      --mode=tex                 \
	      check $(file) ;            \
        )

check: 	$(main).pdf
	-grep --color=always --ignore-case Warning $(main).log
	-grep --color=always --ignore-case erfull $(main).log

view : $(main).pdf
	okular $< &

linear : $(main).pdf
	qpdf --linearize $< linearized.pdf
	mv linearized.pdf $<

meta : $(main).pdf
	exiftool -a -G1 $(main).pdf

################################################################

# Convert SVG to PDF

SVG2PDF = /usr/bin/inkscape --export-area-drawing

%.pdf : %.svg
	$(SVG2PDF) --file=$< --export-pdf=$@

################################################################

# Here is how to LaTeX the document

LATEX = pdflatex

$(main).pdf : $(main).ltx        \
	      $(main).xmpdata    \
	      altacv.cls         \
	      altacv_conf.sty    \
	      references.bib     \
	      $(svg:%.svg=%.pdf) \
	      $(pdf) \
	      $(png) \
	      $(jpg) \
	      $(tex)
	$(LATEX) $<
	biber $(main)
	$(LATEX) $<
	$(LATEX) $<

################################################################

# Run in --silent mode unless the user sets VERBOSE=1 on the                    
# command-line.                                                                 

ifndef VERBOSE
.SILENT:
endif
